export const projects = [
    {
        name: 'GSTPCTO',
        image: 'https://i.imgur.com/fqjkUlG.png',
        description: 'Il progetto finale che racchiude tutta la nostra esperienza come Web Developers. Un progetto che si propone di gestire in modo semplice e digitale il PCTO per la scuola.',
        link: 'https://github.com/gstpcto/gst-pcto-client',
    },
    {
        name: 'This Word Does Not Exist',
        image: 'https://i.imgur.com/tcb6g24.png',
        description: "Script di Python che tramite Web Scraping raccoglie informazioni da thisworddoesnotexist.com di Thomas Dimson, genera un'immagine e la carica su Instagram.",
        link: 'https://www.instagram.com/thisworddoesnotexist/',
    },
    {
        name: 'Windows NT 4.0 / Windows 95 Key Genenerator',
        image: 'https://i.imgur.com/ekGuRqP.jpg',
        description: 'Un generatore di chiavi di attivazione per i sistemi operativi Windows NT 4.0 e Windows 95.',
        link: 'https://www.windows95keygen.tk/',
    },
    {
        name: 'Windows 95 Chat',
        image: 'https://i.imgur.com/k4XTZF3.png',
        description: "Una semplice chat realizzata con Socket.io con l'estetica di Windows 95",
        link: 'https://windows95chat.herokuapp.com/',
    },
    {
        name: 'Heart Rate Monitor',
        image: 'https://i.imgur.com/DLUOmY9.png',
        description:
            'Applicazione Full Stack MERN per la visualizzazione grafica del mio battito cardiaco, è un progetto formato da 3 parti, 1 server e 2 client diversi (realizzati per far pratica con React.js e Tailwind CSS), tutti presenti sul mio profilo GitHub.',
        link: 'https://heartrate.ml/',
    },
    {
        name: 'jsSnake',
        image: 'https://i.imgur.com/P5FTubG.png',
        description: 'Il classico gioco di Snake riprodotto in JavaScript/jQuery, con classifica globale realizzato con Firebase, giocabile sia da desktop che da mobile.',
        link: 'https://nilaerdna.github.io/jsSnake/',
    },
    {
        name: 'Stop And Wait',
        image: 'https://i.imgur.com/qL1ovvH.png',
        description: "Stop And Wait, è il progetto di TEPSIT realizzato come compito nell'estate 2020, realizzato con Mattia Ferrari e Giacomo Carlo Camellini.",
        link: 'https://nilaerdna.github.io/StopAndWait/',
    },
    {
        name: 'Networking',
        image: 'https://i.imgur.com/EPpXhyD.png',
        description: 'Con il prof. Angiani e il prof. Pontoriero abbiamo imparato a prapare Cavi Ethernet e a configurare Firewall e Port Forwarding su router TPLink.',
        link: 'https://linandrea.netlify.app/networking/5D_organizzazione_laboratorio_in_presenza_sabato_27_marzo_2021_c.542.pdf',
    },
    {
        name: '<Hackathon>: Hacktion! Lascia la tua impronta',
        image: 'https://i.imgur.com/Twg5ZKH.png',
        description: 'Il 23 Novembre 2019 dalle 8 alle 19, ',
        link: 'https://linandrea.netlify.app/hackathon/ManualMente_Locandina.jpg',
    },
    {
        name: 'Cambridge Assessment English',
        image: 'https://i.imgur.com/ApREhs7.png',
        description: 'Con la scuola ho conseguito la certificazione in inglese di livello B2',
        link: 'https://www.linguapoint.it/',
    },
];
