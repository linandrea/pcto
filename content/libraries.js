export const libraries = [
  [
    {
      name: "jQuery",
      perc: "75",
    },
    {
      name: "Express.js",
      perc: "55",
    },
    {
      name: "Socket.io",
      perc: "60",
    },
    {
      name: "React.js",
      perc: "70",
    },
    {
      name: "Next.js",
      perc: "70",
    },
    {
      name: "Flask",
      perc: "45",
    },
  ],
  [
    {
      name: "Bulma",
      perc: "75",
    },
    {
      name: "Bootstrap",
      perc: "60",
    },
    {
      name: "Tailwind CSS",
      perc: "65",
    },
    {
      name: "Material-UI",
      perc: "70",
    },
  ],
];
