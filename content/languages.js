export const languages = [
  [
    {
      name: "C++",
      perc: "45",
    },
    {
      name: "Java",
      perc: "60",
    },
    {
      name: "PHP",
      perc: "75",
    },
    {
      name: "Python",
      perc: "70",
    },
  ],
  [
    {
      name: "HTML",
      perc: "95",
    },
    {
      name: "CSS/Sass",
      perc: "70",
    },
    {
      name: "JavaScript",
      perc: "75",
    },
  ],
];
