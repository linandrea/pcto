export const classe5Projects = [
    {
        companyName: 'Git',
        logoSrc: 'https://i.imgur.com/xA37LTI.png',
        websiteName: 'github.com/allemonta',
        websiteLink: 'https://github.com/allemonta',
        description: "Git è un sistema di versionamento gratuito e open source progettato per gestire qualsiasi cosa, dai progetti piccoli a quelli molto grandi con velocità ed efficienza.",
        timestamp: '08:00 - 30 ott 2020',
        projectRoute: 'git',
    },
    {
        companyName: 'Unicode',
        logoSrc: 'https://i.imgur.com/6cgeAaK.png',
        websiteName: 'unicode.it',
        websiteLink: 'https://www.unicode.it/',
        description: "Siamo un'azienda specializzata in soluzioni software moderne per soddisfare le vostre esigenze. Una vasta gamma di servizi vi accompagneranno a raggiungere i vostri obiettivi.",
        timestamp: '08:00 - 13 nov 2020',
        projectRoute: 'unicode',
    },
];
