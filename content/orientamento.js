export const orientamento = [
  {
    companyName: "Matteo Corradini",
    logoSrc: "https://i.imgur.com/IcW9cWI.jpg",
    websiteName: "linkedin.com/matteo-corradini",
    websiteLink: "https://www.linkedin.com/in/matteo-corradini/",
    description:
      "Matteo Corradini è un hacker etico, studente ed atleta, ci ha presentato i possibili percorsi dopo la scuola superiore nel campo della sicurezza informatica.",
    timestamp: "11:00 - 23 dic 2020",
  },
  {
    companyName: "UNIMORE",
    logoSrc: "https://i.imgur.com/7YPgNSe.png",
    websiteName: "unimore.it",
    websiteLink: "https://www.unimore.it/",
    description:
      "Università degli studi di Modena e Reggio Emilia, ci hanno presentato il percorso della Magistrale in Informatica.",
    timestamp: "12:00 - 23 dic 2020",
  },
  {
    companyName: "ITS Maker",
    logoSrc: "https://i.imgur.com/a8aZKHl.png",
    websiteName: "itsmaker.it",
    websiteLink: "https://itsmaker.it/",
    description:
      "ITS MAKER è l'Istituto Superiore di Meccanica, Meccatronica, Motoristica e Packaging dell'Emilia Romagna. Realizza percorsi biennali post diploma d'eccellenza, finalizzati all'ingresso nelle migliori aziende meccaniche e meccatroniche dell'Emilia Romagna.",
    timestamp: "15:30 - 22 feb 2021",
  },
  {
    companyName: "Elettric80",
    logoSrc: "https://i.imgur.com/ElEZ3GJ.png",
    websiteName: "elettric80.com",
    websiteLink: "https://www.elettric80.com/",
    description:
      "Elettric80, fondata negli anni '80 a Viano, in provincia di Reggio Emilia, è specializzata nella realizzazione di soluzioni logistiche automatizzate per le imprese produttrici di beni di largo consumo nei settori beverage, food, tissue ed in ambiti diversificati.",
    timestamp: "15:30 - 22 feb 2021",
  },
  {
    companyName: "ER.GO",
    logoSrc: "https://i.imgur.com/YiNq3UR.png",
    websiteName: "er-go.it",
    websiteLink: "https://www.er-go.it/",
    description:
      "Er.Go è l'Azienda Regionale per il Diritto agli Studi Superiori dell'Emilia Romagna ed offre servizi a studenti e neolaureati delle Università e degli Istituti dell'alta formazione artistica e musicale dell'Emilia -Romagna, studenti e neolaureati stranieri inseriti in programmi di mobilità internazionale e di ricerca, a ricercatori e professori provenienti da altre Università o istituti di ricerca italiani o stranieri.",
    timestamp: "11:00 - 15 mar 2021",
  },
];
