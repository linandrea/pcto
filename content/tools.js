export const tools = [
  [
    {
      name: "Git",
      perc: "95",
    },
    {
      name: "MySQL",
      perc: "70",
    },
    {
      name: "MongoDB",
      perc: "60",
    },
    {
      name: "Firebase",
      perc: "40",
    },
  ],
];
