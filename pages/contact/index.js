import Section from "@components/Section";
import Hero from "@components/Hero";
import { MdEmail } from "react-icons/md";
import {
  FaEnvelope,
  FaGithubAlt,
  FaInstagram,
  FaTwitter,
  FaLinkedin,
} from "react-icons/fa";
import { useForm } from "react-hook-form";
import { useState } from "react";
import axios from "axios";

export default function Contact() {
  const { register, handleSubmit, watch, errors } = useForm();

  const [buttonStyle, setButtonStyle] = useState({
    loading: "",
    disabled: false,
    color: "is-link",
    content: "Inviami un'email",
  });

  const onSubmit = (data) => {
    console.log(data);

    setButtonStyle({
      loading: "is-loading",
      disabled: true,
      color: "is-info",
      content: "Loading",
    });

    axios
      .post("/api/email", data)
      .then((response) => {
        console.log(response);
        setButtonStyle({
          loading: "",
          disabled: true,
          color: "is-success",
          content: "Email inviata, risponderò il prima possibile!",
        });
      })
      .catch((error) => {
        console.log(error);
        setButtonStyle({
          loading: "",
          disabled: true,
          color: "is-danger",
          content: "C'è stato un errore, riprova più tardi.",
        });
      });
  };

  return (
    <>
      <Hero text="Contattami">
        <MdEmail className="icon is-medium is-dark" />
      </Hero>

      <Section>
        <div className="columns">
          <div className="column is-half">
            <form onSubmit={handleSubmit(onSubmit)}>
              <div className="control has-icons-left has-icons-right">
                <input
                  name="contactEmail"
                  className="input"
                  type="email"
                  placeholder="Email"
                  ref={register({ required: true })}
                />
                <span className="icon is-small is-left">
                  <FaEnvelope />
                </span>
                <span className="icon is-small is-right">
                  <i className="fas fa-check" />
                </span>
              </div>
              <div className="control mt-2 mb-2">
                <textarea
                  name="emailText"
                  className="textarea"
                  rows={10}
                  placeholder="Contattami se hai qualche proposta da farmi!"
                  ref={register({ required: true })}
                />
              </div>
              <div className="control">
                <button
                  className={`${buttonStyle.loading} button ${buttonStyle.color}`}
                  type="submit"
                  disabled={buttonStyle.disabled}
                >
                  {buttonStyle.content}
                </button>
              </div>
            </form>
          </div>
          <div className="column is-half">
            <div>
              <p className="title">Altri contatti</p>
              <p className="is-size-5">
                <FaEnvelope className="icon is-medium p-1" />
                <a
                  href="mailto:andreaziliang.lin@studenti.iispascal.it?subject=Ciao Andrea! Abbiamo una proposta interessante per te."
                  rel="noopener noreferrer"
                  target="_blank"
                >
                  andreaziliang.lin@studenti.iispascal.it
                </a>
              </p>
              <p className="is-size-5">
                <FaGithubAlt className="icon is-medium p-1" />
                <a
                  href="https://github.com/nilaerdna"
                  rel="noopener noreferrer"
                  target="_blank"
                >
                  @nilaerdna
                </a>
              </p>
              <p className="is-size-5">
                <FaLinkedin className="icon is-medium p-1" />
                <a
                  href="https://www.linkedin.com/in/andrea-lin-583653206/"
                  rel="noopener noreferrer"
                  target="_blank"
                >
                  LinkedIn
                </a>
              </p>
              <p className="is-size-5">
                <FaTwitter className="icon is-medium p-1" />
                <a
                  href="https://twitter.com/nilaerdna"
                  rel="noopener noreferrer"
                  target="_blank"
                >
                  @nilaerdna
                </a>
              </p>
              <p className="is-size-5">
                <FaInstagram className="icon is-medium p-1" />
                <a
                  href="https://www.instagram.com/andreazllin/"
                  rel="noopener noreferrer"
                  target="_blank"
                >
                  @andreazllin
                </a>
              </p>
            </div>
          </div>
        </div>
      </Section>
    </>
  );
}
