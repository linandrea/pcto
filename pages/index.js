import Section from "@components/Section";
import Hero from "@components/Hero";
import OtherCard from "@components/OtherCard";
import { SiFlask } from "react-icons/si";
import {
  IoHardwareChip,
  IoCodeSlash,
  IoLogoReact,
  IoLogoTux,
  IoLanguage,
} from "react-icons/io5";
import { languages } from "@content/languages";
import { libraries } from "@content/libraries";
import { tools } from "@content/tools";
import Progress from "@components/ProgressWrapper";

export default function Home() {
  const birthday = new Date("2002-10-31");

  const calculateAge = (birthday) => {
    let difference = Date.now() - birthday.getTime();
    let date = new Date(difference);
    return Math.abs(date.getUTCFullYear() - 1970);
  };

  const age = calculateAge(birthday);

  return (
      <>
          <Hero text="Andrea Lin">
              <img src="/assets/Logo.svg" alt="School Logo" className="icon is-medium" />
          </Hero>
          <Section>
              <div className="tile is-ancestor">
                  <div className="tile is-parent">
                      <article className="is-child ">
                          <div className="card fh">
                              <div className="card-image">
                                  <figure className="image is-square">
                                      <img src="https://i.imgur.com/TgxmYrp.jpg" alt="Questo sono io!" />
                                  </figure>
                              </div>
                              <div className="card-content">
                                  <div className="media">
                                      <div className="media-content">
                                          <p className="title is-4">Andrea Lin</p>
                                          <p className="subtitle is-6">@nilaerdna</p>
                                      </div>
                                  </div>
                                  <div className="content">
                                      Ciao, mi chiamo Andrea Lin, ho {age} anni e frequento l'indirizzo informatico all'I.I.S. Blaise Pascal di Reggio Emilia. In futuro vorrei studiare informatica all'università e poi lavorare come Web Developer.
                                  </div>
                              </div>
                          </div>
                      </article>
                  </div>
                  <div className="tile is-vertical is-8">
                      <div className="tile is-parent">
                          <article className="tile is-child">
                              <div className="content">
                                  <Progress title="Linguaggi" data={languages} />
                              </div>
                          </article>
                      </div>
                      <div className="tile is-parent">
                          <article className="tile is-child">
                              <div className="content">
                                  <Progress title="Librerie e Frameworks" data={libraries} />
                              </div>
                          </article>
                      </div>
                      <div className="tile is-parent">
                          <article className="tile is-child">
                              <div className="content">
                                  <Progress title="Strumenti e Servizi" data={tools} />
                              </div>
                          </article>
                      </div>
                  </div>
              </div>
          </Section>
          <Section>
              <div className="columns is-multiline is-centered">
                  <div className="column is-one-third">
                      <OtherCard icon={<IoHardwareChip className="has-text-primary icon is-medium" />} title="Hardware">
                          <p className="subtitle">Mi piace avere qualsiasi gadget tecnologico sotto mano, io stesso mi sono assemblato il mio computer!</p>
                      </OtherCard>
                  </div>
                  <div className="column is-one-third">
                      <OtherCard icon={<IoLogoReact className="has-text-info icon is-medium" />} title="MERN">
                          <p className="subtitle">Attualmente sono immerso nel mondo del Web Development, con lo stack MERN.</p>
                      </OtherCard>
                  </div>
                  <div className="column is-one-third">
                      <OtherCard icon={<IoCodeSlash className="has-text-success icon is-medium" />} title="Coding">
                          <p className="subtitle">Ho iniziato a scrivere codice all'età di 13 anni, e da lì mi sono innamorato della materia.</p>
                      </OtherCard>
                  </div>
                  <div className="column is-one-third">
                      <OtherCard icon={<IoLogoTux className="has-text-warning icon is-medium" />} title="LAMP">
                          <p className="subtitle">A scuola abbiamo imparato come creare da 0 una web application con lo stack LAMP.</p>
                      </OtherCard>
                  </div>
                  <div className="column is-one-third">
                      <OtherCard icon={<IoLanguage className="has-text-danger icon is-medium" />} title="Inglese">
                          <p className="subtitle">Attualmente sto studiando per la certificazione B2 in inglese.</p>
                      </OtherCard>
                  </div>
                  <div className="column is-one-third">
                      <OtherCard icon={<SiFlask className="has-text-grey icon is-medium" />} title="Flask">
                          <p className="subtitle">Con un corso extracurricolare abbiamo potuto sperimentare con Python e un MVC.</p>
                      </OtherCard>
                  </div>
              </div>
          </Section>
      </>
  );
}
