import React from "react";
import Hero from "@components/Hero";
import Section from "@components/Section";

export default function Unicode() {
  return (
    <>
      <Hero text="Git">
        <img
          src="/assets/Logo.svg"
          alt="School Logo"
          className="icon is-medium"
        />
      </Hero>
      <Section>
        <div className="columns">
          <div className="column">
            <p>30 ottobre 2020 - Classe 5<sup>a</sup>D.</p>
            <br />
            <h1 className="title is-size-4">Cosa abbiamo fatto?</h1>
            <p className="subtitle is-size-6">
              Un esperto esterno, Alessandro Montanari, ex studente del Pascal ci ha spiegato come utilizzare Git e hostare una repo su GitHub. 
            </p>
            <hr />
            <h1 className="title is-size-4">Conclusioni</h1>
            <p className="subtitle is-size-6">
              Personalmente l'attività anche se interessante non mi è servita perché utilizzavo già Git per progetti personali.
            </p>
          </div>
        </div>
      </Section>
    </>
  );
}
