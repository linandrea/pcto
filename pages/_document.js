import Document, { Html, Head, Main, NextScript } from "next/document";

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps };
  }

  render() {
    return (
      <Html lang="it">
        <Head>
          <meta charSet="utf-8" />
          <meta
            name="description"
            content="Ciao, sono Andrea Lin, attualmente sono uno studente, 
                    aspriro a diventare un Full Stack Developer, 
                    questo è il mio portfolio  in cui sono raccolti vari progetti scolastici e personali."
          />
          <meta
            name="keywords"
            content="portfolio, andrea zi liang lin, andrea ziliang lin, andrea zi liang, andrea ziliang, andrea, zi liang, ziliang, andrea lin, zi liang lin, lin, web-dev, mern"
          />
          <meta name="author" content="Andrea Lin" />
          <link rel="shurtcut icon" href="/favicon.ico" />
        </Head>
        <body className="has-navbar-fixed-top">
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
