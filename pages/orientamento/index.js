import React from "react";
import Hero from "@components/Hero";
import SchoolProjectRow from "@components/SchoolProjectRow";
import Section from "@components/Section";
import { orientamento } from "@content/orientamento";
import { ImCompass2 } from "react-icons/im";

export default function Orientamento() {
  return (
    <>
      <Hero text="Orientamento">
        <img
          src="/assets/Logo.svg"
          alt="School Logo"
          className="icon is-medium"
        />
      </Hero>
      <Section>
        <article className="message is-info">
          <div className="message-body">
            <p>
              <ImCompass2 className="icon is-small" /> Queste sono le attività
              di orientamento che abbiamo fatto durante gli ultimi anni.
            </p>
          </div>
        </article>
        {orientamento.map((project, index) => {
          return <SchoolProjectRow key={index} project={project} />;
        })}
      </Section>
    </>
  );
}
