import "@styles/globals.css";
import "@styles/styles.scss";
import Navbar from "@components/Navbar";
import Footer from "@components/Footer";
import Head from "next/head";
import { IconContext } from "react-icons";

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Head>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta charSet="utf-8" />
        <meta
          name="description"
          content="Ciao, sono Andrea Lin, attualmente sono uno studente, 
                    aspriro a diventare un Full Stack Developer, 
                    questo è il mio portfolio  in cui sono raccolti vari progetti scolastici e personali."
        />
        <meta
          name="keywords"
          content="portfolio, andrea zi liang lin, andrea ziliang lin, andrea zi liang, andrea ziliang, andrea, zi liang, ziliang, andrea lin, zi liang lin, lin, web-dev, mern"
        />
        <meta name="author" content="Andrea Lin" />
        <link rel="shurtcut icon" href="/favicon.ico" />
        <title>Andrea Lin</title>
      </Head>
      <Navbar />
      <IconContext.Provider value={{ style: { verticalAlign: "middle" } }}>
        <Component {...pageProps} />
      </IconContext.Provider>
      <Footer />
    </>
  );
}

export default MyApp;
