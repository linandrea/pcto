import React from 'react';
import Hero from '@components/Hero';
import Section from '@components/Section';

export default function Accenture() {
    return (
        <>
            <Hero text="Accenture">
                <img src="/assets/Logo.svg" alt="School Logo" className="icon is-medium" />
            </Hero>
            <Section>
                <div className="columns">
                    <div className="column">
                        <p>
                            <small>
                                9 aprile 2019 - Classe 3<sup>a</sup>D.
                            </small>
                        </p>
                        <br />
                        <div className="content">
                            <h1 className="title is-size-4">Cosa abbiamo fatto?</h1>
                            <p className="subtitle is-size-6">
                                Nella prima parte della visita ci hanno diviso in gruppi e ci hanno assegnato a varie persone che svolgevano varie mansioni all'interno dell'azienda. Abbiamo avuto la possibilità di vedere al lavoro uno dei
                                programmatori che ci ha spiegato come funzionava il workflow all'interno della sua sezione. Nella seconda parte della visita, con tutta la classe ci hanno raccontato della storia dell'azienda e dei vari settori in cui
                                lavora. Altri argomenti di cui ci hanno parlato sono:
                            </p>
                            <p className="subtitle is-size-6">
                                <ul>
                                    <li>"Terna (ex. GRTN)" e come gestirono il blackout nazionale del 2003.</li>
                                    <li>Blockchain e come funziona.</li>
                                    <li>Funzionamento di UiPath, un programma di automazione.</li>
                                    <li>Intelligenze artificiali e Machine Learning.</li>
                                </ul>
                            </p>
                            <p className="subtitle is-size-6">
                                Durante la presentazione ci hanno fatto provare vari robot che parlavano e interagivano con noi. Alla fine abbiamo anche svolto un gioco in cui tramite un programma/linguaggio simil-Scratch dovevamo far eseguire dei
                                movimenti ad un braccio robotico cercando di ricreare la disposizione di alcuni cubi colorati mostrati in una foto.
                            </p>
                            <hr />
                            <h1 className="title is-size-4">Conclusioni</h1>
                            <p className="subtitle is-size-6">
                              Dalla spiegazione di uno dei dipendenti ho imparato che il oltre alle capacità individuali è importatissimo saper lavorare in team, 
                              ho anche scoperto del blackout del 2003 e di programmi a me sconosciuti come UiPath e ho approffondito le mie conoscenze sulla Blockchain e sulle AI.
                            </p>
                        </div>
                        <div className="columns">
                            <div className="column">
                                <a className="button is-primary is-outlined is-fullwidth is-hoverable" rel="noopener noreferrer" target="_blank" href="https://linandrea.netlify.app/accenture/robot.mp4">
                                    Video
                                </a>
                            </div>
                            <div className="column">
                                <a className="button is-link is-outlined is-fullwidth is-hoverable" rel="noopener noreferrer" target="_blank" href="https://linandrea.netlify.app/accenture/accenture.jpeg">
                                    Certificato di partecipazione
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </Section>
        </>
    );
}
