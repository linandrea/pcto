import React from "react";
import {
  SiBulma,
  SiReact,
  SiReactrouter,
  SiNextDotJs,
  SiNetlify,
} from "react-icons/si";
export default function Footer() {
  const y = new Date().getFullYear();
  return (
    <footer className="footer">
      <div className="content has-text-centered">
        <p>
          <strong>Portfolio PCTO</strong> di <strong>Andrea Lin</strong>
          <br />
          IIS Blaise Pascal, RE. 2018 - {y}.
        </p>
        <SiBulma className="icon is-medium px-1" />
        <SiReact className="icon is-medium px-1" />
        <SiNextDotJs className="icon is-medium px-1" />
        <SiNetlify className="icon is-medium px-1" />
      </div>
    </footer>
  );
}
