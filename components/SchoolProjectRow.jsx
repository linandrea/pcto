import React from "react";
import Link from "next/link";
import { useRouter } from "next/router";

export default function SchoolProjectRow({ project }) {
  const { pathname } = useRouter();
  const {
    companyName,
    logoSrc,
    websiteName,
    websiteLink,
    description,
    timestamp,
    projectRoute,
  } = project;
  return (
    <article className="media">
      <figure className="media-left">
        <p className="image is-128x128">
          {projectRoute ? (
            <Link href={`${pathname}/${projectRoute}`}>
              <img
                className="is-rounded hover-effect"
                alt="Company Logo"
                src={logoSrc}
              />
            </Link>
          ) : (
            <img className="is-rounded" alt="Company Logo" src={logoSrc} />
          )}
        </p>
      </figure>
      <div className="media-content">
        <div className="content">
          <p>
            <strong>{companyName}</strong>{" "}
            <small>
              <a target="_blank" rel="noopener noreferrer" href={websiteLink}>
                {websiteName}
              </a>
            </small>{" "}
            <small>{timestamp}</small>
            <br />
            {description}
          </p>
        </div>
      </div>
    </article>
  );
}
