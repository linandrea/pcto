import React, { useState, Children } from "react";
import Link from "next/link";
import { withRouter } from "next/router";

export default function Navbar() {
  const [isActive, setActive] = useState("");

  const handleActiveToggle = () => {
    if (isActive === "") {
      setActive("is-active");
    } else {
      setActive("");
    }
  };

  return (
    <nav className="navbar is-fixed-top" role="navigation">
      <div className="navbar-brand">
        <Link href="/">
          <a className="navbar-item">
            <img src="/assets/Logo.svg" alt="Logo" />
          </a>
        </Link>

        <a
          role="button"
          className={"navbar-burger " + isActive}
          onClick={handleActiveToggle}
        >
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
        </a>
      </div>

      <div className={"navbar-menu " + isActive}>
        <div className="navbar-start">
          <ActiveLink href="/">
            <a>Home</a>
          </ActiveLink>
          <ActiveLink href="/3">
            <a>A.S. 2018/2019</a>
          </ActiveLink>
          <ActiveLink href="/4">
            <a>A.S. 2019/2020</a>
          </ActiveLink>
          <ActiveLink href="/5">
            <a>A.S. 2020/2021</a>
          </ActiveLink>
          <ActiveLink href="/orientamento">
            <a>Orientamento</a>
          </ActiveLink>
          <ActiveLink href="/other">
            <a>Altri Progetti</a>
          </ActiveLink>
          <ActiveLink href="/contact">
            <a>Contattami</a>
          </ActiveLink>
        </div>
      </div>
    </nav>
  );
}

const ActiveLink = withRouter(({ router, children, ...props }) => (
  <Link {...props}>
    {React.cloneElement(Children.only(children), {
      className:
        `/${router.pathname.split("/")[1]}` === props.href
          ? "navbar-item is-active"
          : "navbar-item",
    })}
  </Link>
));
