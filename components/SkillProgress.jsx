import React, { useState } from "react";

export default function SkillProgress({ section }) {
  const [element, setElement] = useState(section[0]);
  return (
    <div className="content">
      <div className="buttons are-small m-0">
        {section.map((skill, index) => (
          <button
            className="button"
            key={index}
            onClick={() => setElement(skill)}
          >
            {skill.name}
          </button>
        ))}
      </div>
      <span>
        {element.name}: {element.perc}%
      </span>
      <progress
        className="progress is-small mt-1"
        value={element.perc}
        max={100}
      >
        {element.perc}%
      </progress>
    </div>
  );
}
