import React from "react";
import SkillProgress from "@components/SkillProgress";

export default function Progress({ title, data }) {
  return (
    <div className="container">
      <h2 className="title is-size-4">{title}</h2>
      {data.map((section, index) => (
        <SkillProgress section={section} key={index} />
      ))}
    </div>
  );
}
